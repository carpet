# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Jun 24 2013 at 10:56:02-0400
# parameter filename: "../../arrangements/Carpet/CarpetReduce/test/periodic_weight.par"
#
# CARPETREDUCE::WEIGHT x (carpetreduce::weight)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:weight
0	0	0 0 0	0 13 13	0	-2.6 0 0	0
0	0	0 0 0	1 13 13	0	-2.4 0 0	0
0	0	0 0 0	2 13 13	0	-2.2 0 0	0
0	0	0 0 0	3 13 13	0	-2 0 0	1
0	0	0 0 0	4 13 13	0	-1.8 0 0	1
0	0	0 0 0	5 13 13	0	-1.6 0 0	1
0	0	0 0 0	6 13 13	0	-1.4 0 0	1
0	0	0 0 0	7 13 13	0	-1.2 0 0	1
0	0	0 0 0	8 13 13	0	-1 0 0	1
0	0	0 0 0	9 13 13	0	-0.8 0 0	1
0	0	0 0 0	10 13 13	0	-0.6 0 0	1
0	0	0 0 0	11 13 13	0	-0.4 0 0	1
0	0	0 0 0	12 13 13	0	-0.2 0 0	1
0	0	0 0 0	13 13 13	0	0 0 0	1
0	0	0 0 0	14 13 13	0	0.2 0 0	1
0	0	0 0 0	15 13 13	0	0.4 0 0	1
0	0	0 0 0	16 13 13	0	0.6 0 0	1
0	0	0 0 0	17 13 13	0	0.8 0 0	1
0	0	0 0 0	18 13 13	0	1 0 0	1
0	0	0 0 0	19 13 13	0	1.2 0 0	1
0	0	0 0 0	20 13 13	0	1.4 0 0	1
0	0	0 0 0	21 13 13	0	1.6 0 0	1
0	0	0 0 0	22 13 13	0	1.8 0 0	1
0	0	0 0 0	23 13 13	0	2 0 0	0
0	0	0 0 0	24 13 13	0	2.2 0 0	0
0	0	0 0 0	25 13 13	0	2.4 0 0	0


