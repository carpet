# /*@@
#   @file      wavetoyf77_rad.par
#   @date      2001-03-06
#   @author    Erik Schnetter
#   @desc
#   Wavetoy parameter file demonstrating radiation boundaries in octant mode
#   @enddesc
# @@*/
#
# $Header:$

ActiveThorns = "Boundary IOBasic IOUtil Time Cart3d Slab Carpet CarpetIOASCII CarpetIOFlexIO CarpetLib CarpetReduce CarpetRegrid CarpetSlab IDSpaceTimeToy SpaceTimeToy HydroToy"

Carpet::poison_new_timelevels	= yes
#Carpet::check_for_poison	= yes
Carpet::checksum_timelevels	= yes

Cactus::cctk_itlast		= 120

Time::dtfac			= 0.5

driver::global_nx		= 34
driver::global_ny		= 48
driver::global_nz		= 64

driver::ghost_size		= 2

grid::type			= byrange
grid::xmin			= -5.1
grid::ymin			= -6.9
grid::zmin			= -9.3
grid::xmax			=  4.8
grid::ymax			=  7.2
grid::zmax			=  9.6
grid::mode			= periodic

IO::out_dir			= "spacetimetoy_periodic"

IOBasic::outinfo_every		= 1 # 10
IOBasic::outinfo_vars		= "spacetimetoy::spacetimeevolve"

IOBasic::outScalar_every	= 1 # 2
IOBasic::outScalar_vars		= "spacetimetoy::spacetimeevolve"

IOASCII::out1D_every		= 1 # 2
IOASCII::out1D_vars		= "spacetimetoy::spacetimeevolve grid::coordinates"

#IOFlexIO::out3D_every		= 24
#IOFlexIO::out3D_vars		= "spacetimetoy::spacetimeevolve"
#IOFlexIO::out3D_format		= IEEE
#IOFlexIO::out3D_extension	= ".vcamr"

SpaceTimeToy::icn_iterations	= 2
SpaceTimeToy::bound		= radiation

HydroToy::hydrotoy_active	= no

IDSpaceTimeToy::initial_data	= plane
IDSpaceTimeToy::kx		= 0.222222222222222	# 2 / ((32-2) * 0.3)
IDSpaceTimeToy::ky		= 0.151515151515152	# 2 / ((46-2) * 0.3)
IDSpaceTimeToy::kz		= 0.222222222222222	# 4 / ((62-2) * 0.3)
