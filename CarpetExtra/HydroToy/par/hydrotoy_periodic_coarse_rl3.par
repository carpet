# /*@@
#   @file      wavetoyf77_rad.par
#   @date      2001-03-06
#   @author    Erik Schnetter
#   @desc
#   Wavetoy parameter file demonstrating radiation boundaries in octant mode
#   @enddesc
# @@*/
#
# $Header:$

ActiveThorns = "Boundary IOBasic IOUtil Time Cart3d Carpet CarpetIOASCII CarpetIOFlexIO CarpetLib CarpetReduce CarpetRegrid CarpetSlab IDHydroToy HydroToy"

Carpet::poison_new_timelevels	= yes
Carpet::check_for_poison	= yes
Carpet::checksum_timelevels	= yes

Cactus::cctk_itlast		= 240

Time::dtfac			= 0.5

driver::global_nx		= 19
driver::global_ny		= 26
driver::global_nz		= 34

driver::ghost_size		= 2

Carpet::max_refinement_levels	= 3
Carpet::prolongation_order_space= 3
Carpet::prolongation_order_time	= 2

CarpetRegrid::refinement_levels	= 3

grid::type			= byrange
grid::xmin			= -5.4
grid::ymin			= -7.2
grid::zmin			= -9.6
grid::xmax			=  5.4
grid::ymax			=  7.8
grid::zmax			= 10.2
grid::mode			= periodic

IO::out_dir			= "hydrotoy_periodic_coarse_rl3"

IOBasic::outinfo_every		= 1 # 10
IOBasic::outinfo_vars		= "hydrotoy::hydroevolve"

IOBasic::outScalar_every	= 1 # 2
IOBasic::outScalar_vars		= "hydrotoy::hydroevolve"

IOASCII::out1D_every		= 1 # 2
IOASCII::out1D_vars		= "hydrotoy::hydroevolve grid::coordinates"

IOFlexIO::out3D_every		= 48
IOFlexIO::out3D_vars		= "hydrotoy::hydroevolve"
IOFlexIO::out3D_format		= IEEE
IOFlexIO::out3D_extension	= ".vcamr"

HydroToy::bound			= radiation

IDHydroToy::initial_data	= plane
IDHydroToy::kx			= 0.222222222222222	# 2 / ((32-2) * 0.3)
IDHydroToy::ky			= 0.151515151515152	# 2 / ((46-2) * 0.3)
IDHydroToy::kz			= 0.222222222222222	# 4 / ((62-2) * 0.3)
