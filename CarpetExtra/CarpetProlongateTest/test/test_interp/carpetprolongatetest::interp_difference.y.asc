# 1D ASCII output created by CarpetIOASCII
# created on erik-schnetters-macbook-pro.local by eschnett on Mar 07 2011 at 14:52:05-0500
# parameter filename: "../../arrangements/CarpetExtra/CarpetProlongateTest/test/test_interp.par"
#
# CARPETPROLONGATETEST::INTERP_DIFFERENCE y (carpetprolongatetest::interp_difference)
#
# iteration 0
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:interp_x 14:interp_y 15:interp_z 16:interp_u 17:interp_u0 18:interp_du
0	0 0 0 0	0 0 0	0	0 0 0	-0.5 -0.5 -0.5 -127.001953125 -127.001953125 0
0	0 0 0 0	0 1 0	0	0 1 0	-0.5 -0.5 -0.5 -127.001953125 -127.001953125 0
0	0 0 0 0	0 2 0	0	0 2 0	-0.5 -0.5 -0.5 -127.001953125 -127.001953125 0
0	0 0 0 0	0 3 0	0	0 3 0	-0.5 -0.5 -0.5 -127.001953125 -127.001953125 0


# iteration 256
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:interp_x 14:interp_y 15:interp_z 16:interp_u 17:interp_u0 18:interp_du
256	0 0 0 0	0 0 0	0.5	0 0 0	-0.5 -0.5 -0.5 -127.250488288701 -127.250488288701 0
256	0 0 0 0	0 1 0	0.5	0 1 0	-0.5 -0.5 -0.5 -127.250488288701 -127.250488288701 0
256	0 0 0 0	0 2 0	0.5	0 2 0	-0.5 -0.5 -0.5 -127.250488288701 -127.250488288701 0
256	0 0 0 0	0 3 0	0.5	0 3 0	-0.5 -0.5 -0.5 -127.250488288701 -127.250488288701 0


# iteration 512
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:interp_x 14:interp_y 15:interp_z 16:interp_u 17:interp_u0 18:interp_du
512	0 0 0 0	0 0 0	1	0 0 0	-0.5 -0.5 -0.5 -381.005859375 -381.005859375 0
512	0 0 0 0	0 1 0	1	0 1 0	-0.5 -0.5 -0.5 -381.005859375 -381.005859375 0
512	0 0 0 0	0 2 0	1	0 2 0	-0.5 -0.5 -0.5 -381.005859375 -381.005859375 0
512	0 0 0 0	0 3 0	1	0 3 0	-0.5 -0.5 -0.5 -381.005859375 -381.005859375 0


